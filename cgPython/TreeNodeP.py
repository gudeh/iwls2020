import pandas as pd
import numpy as np
from cgp import CGP
#from graphviz import Digraph
import uuid
from multiprocessing import Process, Queue

class TreeNode:

    def __init__(self, df, parent=None):
        self.nodeId=uuid.uuid1()
        self.parent = parent
        self.data = df
        self.numInputs = df.shape[1] - 1 if self.parent is None else self.parent.numInputs
        self.indexes = None
        self.leftNode = None
        self.rightNode = None
        self.cgp = None
        self.listLeaves=[]

        print("Node created with %i instances and %i columns - total inputs is always %i "%(len(self.data),len(self.data.columns),self.numInputs))

        if parent is None:
            self.stopDivAt = 1800
            self.cgpLayers = 20
            self.cgpNumGen = 5000
            self.cgpNumSeeds = 10
            self.numProcTree = 1
            self.numProcSeed = 10 #careful because the total is numProcTree x numProcSeed
        else:
            self.stopDivAt = self.parent.stopDivAt
            self.cgpLayers = self.parent.cgpLayers
            self.cgpNumGen = self.parent.cgpNumGen
            self.cgpNumSeeds = self.parent.cgpNumSeeds
            self.numProcTree = self.parent.numProcTree
            self.numProcSeed = self.parent.numProcSeed

        self.cgpBest = None

    def registerLeaf(self):
        node = self
        while not node.parent is None: #go up to the root
            node = node.parent
        #root found
        node.listLeaves.append(self)

    def trainLeaves(self):
        if self.parent is None:
            def workerTree(input, output):
                for treeNode in iter(input.get, 'STOP'):
                    try:
                        print(
                            "===== Training a tree node with %i instances and %i columns - original number of inputs= %i === " % (
                            treeNode.data.shape[0], treeNode.data.shape[1], treeNode.numInputs))
                        treeNode.evolveCgp()
                    except Exception as e:
                        print(e)
                    output.put(treeNode)
            taskTreeQueue = Queue()
            doneTreeQueue = Queue()
            procsTree = []

            for i in range(self.numProcTree):
                proc = Process(target=workerTree, args=(taskTreeQueue, doneTreeQueue))
                procsTree.append(proc)
                proc.start()

            cnt=1
            for leaf in self.listLeaves:
                taskTreeQueue.put(leaf)
                cnt+=1

            for i in range(len(self.listLeaves)): #getResults
                node = doneTreeQueue.get();
                self.setCGPToNode(node.nodeId, node.cgp)

            for i in range(self.numProcTree):
                taskTreeQueue.put('STOP')
        else:
            print("TRAIN LEAVES CAN BE CALLED ONLY FROM ROOT!")

    def getNode(self,id):
        result=None
        if self.nodeId == id:
            result=self
        else:
            if not self.leftNode is None:
                result=self.leftNode.getNode(id)
                if result is None and not self.rightNode is None:
                    result=self.rightNode.getNode(id)

        return result


    def setCGPToNode(self,id,cgp):
        node = self.getNode(id)
        if node is None:
            print("NODE ",id," NOT FOUND")
        else:
            node.cgp = cgp
            node.cgpBest = cgp.statistics[-1][0] if cgp.statistics[-1][0]!=0 else 1 #evolution stops when a solution is found

    def reduceAndDivide(self):
        sameValue = np.zeros([self.numInputs, self.numInputs])

        for idCol1 in self.data.columns.values[:-1]:
            for idCol2 in self.data.columns.values[:-1]:
                sameValue[idCol1][idCol2] = 0 if idCol1 == idCol2 else sum(self.data[idCol1] == self.data[idCol2])

        ind1 = np.argmax(np.max(sameValue, axis=0))
        ind2 = np.argmax(sameValue[ind1])

        df1,df2 = self.divideDataByIndex(self.data,ind1,ind2) #df1 is reduced by one dimension

        self.indexes = (ind1, ind2)
        print("Dataset divided by same values on ",self.indexes)
        self.leftNode = TreeNode(df1, self)
        self.leftNode.solve()
        self.rightNode = TreeNode(df2, self)
        self.rightNode.solve()

    def divideDataByIndex(self,data,ind1,ind2):
        dividedDataLeft = data[data[ind1] == data[ind2]].drop([ind2], axis=1)
        dividedDataRight = data[data[ind1] != data[ind2]]
        return dividedDataLeft, dividedDataRight

    def solve(self):
        if len(self.data) < self.stopDivAt:
            if self.data.shape[0]>0: #this is a leaf - only register it to be trained in parallel at the end
                self.registerLeaf()
            else:
                print("THIS DATASET (NODE) IS EMPTY")
        else:
            self.reduceAndDivide();

    def evolveCgp(self):
        def workerSeed(input, output):
            for cgp in iter(input.get, 'STOP'):
                try:
                    print("Seed %i: Layers %i BatchSize %i ChangeEach %i" % (cgp.seed, cgp.numLayers, cgp.miniBatchSize, cgp.changeEach))
                    cgp.evolve()
                except Exception as e:
                    print(e)
                output.put(cgp)

        taskSeedQueue = Queue()
        doneSeedQueue = Queue()

        procsSeed = []
        for i in range(self.numProcSeed):
            proc = Process(target=workerSeed, args=(taskSeedQueue, doneSeedQueue))
            procsSeed.append(proc)
            proc.start()

        cgps = []
        for seed in range(self.cgpNumSeeds):
            cgp = CGP(1, self.cgpLayers, 1, np.array(self.data.values.tolist()), seed)
            cgp.nThreads=1 #careful when changing this cause it could exponentially create processes
            cgp.nGen = self.cgpNumGen
            cgp.miniBatchSize = len(self.data)
            cgp.changeEach = self.cgpNumGen
            cgp.saveResults=False
            cgp.aws=True
            cgps.append(cgp)
            taskSeedQueue.put(cgp)

        bestCgp=0
        bestVal=0
        for i in range(self.cgpNumSeeds):
            cgp = doneSeedQueue.get();
            finalFit = 1 if cgp.solutionFound else cgp.statistics[-1][0]
            if finalFit > bestVal:
                bestVal=finalFit
                bestCgp=cgp
            if not cgp.solutionFound:
                print("SOLUTION NOT FOUND - SEED %i FITNESS %f - BEST SO FAR IS SEED %i FITNESS %f" % (cgp.seed, finalFit,bestCgp.seed,bestVal))
            else:
                print("SOLUTION FOUND Seed %i - dataset size=%i" % (cgp.seed, self.data.shape[0]))
                #for proc in procsSeed:
                #    proc.terminate()
                #taskSeedQueue.close()
                #break

        self.cgp = bestCgp
        self.cgpBest = bestVal

        #if not bestCgp.solutionFound: #if solution was found the queue will be already closed, close it otherwise
        for i in range(self.numProcSeed):
            taskSeedQueue.put('STOP')
        # if not cgp.solutionFound:
        # print("SOLUTION NOT FOUND  - BEST WAS SEED %i FITNESS %f - KEEP DIVIDING..." % (seed, best))
        # self.reduceAndDivide()

    def test(self, data):
        accLeft = 0
        accRight = 0
        if self.leftNode is None:
            # print("Number of instances BEFORE: ", len(root.cgp.dataset))
            self.cgp.dataset = np.array(data.values.tolist()) == 1
            # print("Number of instances that will be tested: ", len(root.cgp.dataset))
            fit, _, cSize = self.cgp.evaluateIndividual(0, self.cgp.pop[0], range(len(self.cgp.dataset)))
            # print("RESULT IS ",fit)
            print("Test done with %i instances - %f accuracy - %i circuit size" % (len(self.cgp.dataset), fit,cSize))
            return len(self.cgp.dataset), fit
        else:
            count = 0
            numInstancesRight = 0
            numInstancesLeft = 0
            dataLeft, dataRight = self.divideDataByIndex(data, self.indexes[0], self.indexes[1])
            print("Data divided %i left %i right" % (len(dataLeft), len(dataRight)))

            if (not self.leftNode is None) and len(dataLeft > 0):
                numInstancesLeft, accLeft = self.leftNode.test(dataLeft)
                count += 1

            if (not self.rightNode is None) and len(dataRight > 0):
                numInstancesRight, accRight = self.rightNode.test(dataRight)
                count += 1

            return (numInstancesLeft + numInstancesRight), (accLeft + accRight) / count