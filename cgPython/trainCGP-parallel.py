import time
import numpy as np
from cgp2 import CGP
from multiprocessing import Process, Queue

#examplesToTrain=[1] #choose the one you want to train
#examplesToTrain=[0:99] #all - comment the previous line and uncomment this one
examplesToTrain=list(range(100))

layers=[100,500,1000]
batchSize=[100,1000,3200,6400]
chEach=[100,500,1000]
nGen=5000
numSeeds=8

numProc=8

def worker(input, output):
  for cgp in iter(input.get, 'STOP'):
    try:
      print("Seed %i: Traning example %s Layers %i BatchSize %i ChangeEach %i" % (cgp.seed, str(cgp.example).zfill(2), cgp.numLayers, cgp.miniBatchSize, cgp.changeEach))
      cgp.evolve()
    except Exception as e:
      print(e)
    output.put(cgp)

task_queue = Queue()
done_queue = Queue()
procs = []
for i in range(numProc):
    proc = Process(target=worker, args=(task_queue, done_queue))
    procs.append(proc)
    proc.start()

numEvol=0
for l in layers:
  for bs in batchSize:
    for ce in chEach:
      for i in examplesToTrain:
        if batchSize==6400:
          if chEach!=100: #do the whole dataset only once and never change it
             continue;
          else:
             ce=10000
        print("Traning example %s Layers %i BatchSize %i ChangeEach %i"%(str(i).zfill(2),l,bs,ce))
        data = np.genfromtxt("data/ex%s.train.csv"%(str(i).zfill(2)),delimiter=",");
        for seed in range(numSeeds):
          cgp = CGP(1,l,1,data,seed);
          cgp.nGen = nGen
          cgp.minMutRate=0.0001
          cgp.resetMutOnChangeBatch=True
          cgp.keepMutRateFor=int(ce*0.3)
          cgp.sag=True
          cgp.aws=False
          cgp.nThreads=1
          cgp.miniBatchSize=bs
          cgp.changeEach=ce
          cgp.example=i
          #cgp.evolve()
          task_queue.put(cgp)
          #print("Best on last generation %f of seed %i"%(cgp.statistics[-1][0],cgp.seed))

cgps=[]
for i in range(numEvol):  # get results
  cgp = done_queue.get();
  cgps.append(cgp)
