import time
import numpy as np
from cgp2 import CGP

#bad_guys = ["ex08.data", "ex42.data", "ex47.data", "ex74.data", "ex06.data", "ex24.data", "ex49.data", "ex44.data", "ex28.data", "ex46.data",
#            "ex48.data", "ex45.data", "ex04.data", "ex26.data", "ex22.data", "ex29.data", "ex02.data", "ex27.data", "ex20.data", "ex90.data",
#            "ex25.data", "ex94.data", "ex78.data", "ex91.data"]

examplesToTrain=[27] #choose the one you want to train
#examplesToTrain=[0:99] #all - comment the previous line and uncomment this one

#layers=[50,100,200,500]
#batchSize=[50,100,1000,3200,6400]
#chEach=[100,500,1000]

layers=[500]
batchSize=[100]
chEach=[1000]
nGen=50000
seedInit=0
numSeeds=10

data = np.genfromtxt("data/ex%s.train.csv" % (str(1).zfill(2)), delimiter=",");

sf=[]
for l in layers:
  for bs in batchSize:
    for ce in chEach:
      for i in examplesToTrain:
        if batchSize==6400:
          if chEach!=100: #do the whole dataset only once and never change it
             continue;
          else:
             ce=10000
        print("Traning example %s Layers %i BatchSize %i ChangeEach %i"%(str(i).zfill(2),l,bs,ce))
        data = np.genfromtxt("data/ex%s.train.csv"%(str(i).zfill(2)),delimiter=",");
        for seed in range(seedInit,seedInit+numSeeds):
          cgp = CGP(1,l,1,data,seed);
          cgp.nGen = nGen
          cgp.initMutRate=0.3
          cgp.minMutRate=0.0001
          cgp.resetMutOnChangeBatch=True
          cgp.keepMutRateFor=int(ce*0.3)
          cgp.nThreads=1
          cgp.miniBatchSize=bs
          cgp.changeEach=ce
          cgp.example=i
          cgp.evolve()
          print("Best on last generation %f of seed %i"%(cgp.bestFit,cgp.genBestFit))
          sf.append(cgp.genBestFit)
print("Generation solution found = Max=%i Mean=%i Min=%i"%(np.max(sf),np.mean(sf),np.min(sf)))
