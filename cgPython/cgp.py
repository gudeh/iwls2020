import numpy as np
from numpy import random as rng
import time
from multiprocessing import Process, Queue
import os
np.set_printoptions(threshold=29406)


class CGP:
    def __init__(self, numNodes, numLayers, popSize, dataMat, seed=99):
        self.numNodes = numNodes
        self.numLayers = numLayers
        self.numTotNodes = numNodes * numLayers + 1
        self.popSize = popSize
        self.fitnessPop = np.zeros([popSize, 1])
        self.p1dataset = dataMat.sum(axis=0)[-1] / len(dataMat)
        self.dataset = dataMat == 1
        self.numInputs = dataMat.shape[1] - 1
        self.numParamsNode = 5  # func i1 j1 i2 j2
        self.numParams = numLayers * numNodes * self.numParamsNode + self.numParamsNode
        self.backlevel = numLayers
        self.pop = np.zeros([self.popSize, self.numParams], dtype=int)
        self.solutionFound=False
        self.replaceWhenCSizeEqual=False

        self.bestFit = 0
        self.solutionThreshold = 0.0001

        # evolution params
        self.nGen = 10000
        self.nOffspring = 4
        self.mutRate = 0.02
        self.minMutRate = 0.001
        self.miniBatchSize = 100
        self.reevalParent = False
        self.changeEach = 500
        self.nThreads = 4
        self.useProbFit = False
        self.useAllFunctions = True  # set to false to use only AND functions
        self.useFunctionalSize = True #consider the circuit size as the number of unique nodes used (True) x the total number of nodes (False)
        self.saveResults=True
        self.aws=True

        self.numFuncNodes=0
        self.listActNodes={}

        self.useAdaFitStoch = False
        self.fitStochInit=0.03
        self.fitStochEnd = 0.001

        self.mutRateIncreaseBy=1.05

        self.seed = seed

        # create functions
        self.functions = []

        self.setUseAllFunctions(self.useAllFunctions)


    def setUseAllFunctions(self, bool=True):
        self.useAllFunctions = bool
        self.numFunctions = 6 if self.useAllFunctions else 2

    def AND(self, x, y):
        return x & y

    def NAND(self, x, y):
        return not (x & y)

    def OR(self, x, y):
        return x | y

    def NOR(self, x, y):
        return not (x | y)

    def XOR(self, x, y):
        return x ^ y

    def NXOR(self, x, y):
        return not (x ^ y)

    def getFunction(self,
                    ind):  # unfortunately lambda functions cannot be used because the multiprocessing lib - they can't be pickled
        if ind == 0:
            return self.AND
        elif ind == 1:
            return self.NAND
        elif ind == 2:
            return self.OR
        elif ind == 3:
            return self.NOR
        elif ind == 4:
            return self.XOR
        elif ind == 5:
            return self.NXOR

    def getNumFunctions(self):
        return self.numFunctions

    def createRandomPop(self):
        for i in range(self.popSize):
            self.pop[i] = self.createIndividual()

    # return a  new or a mutated copy of an individual
    def createIndividual(self, ind=None):
        if ind is None:  # if no individual is provided create a new one - by randomly generating all nodes
            mutInd = np.zeros(self.numParams)
            mutVector = np.zeros(self.numTotNodes * 3)  # 3 because it is input1 input2 and function
        else:
            mutInd = ind.copy()
            # implement mutaiton using percentage
            mutVector = rng.rand(
                self.numTotNodes * 3)  # randonly draw numbers from a normal distribution to define which nodes will be replaced

        mutVector = mutVector < self.mutRate  # Create a boolean vector of the nodes to replace based on the mutation rate
        cnt = 0
        while np.sum(mutVector) == 0:  # be sure that at least one node will be mutated
            cnt += 1
            mutVector = rng.rand(self.numTotNodes * 3) < self.mutRate
            if (cnt > 500):
                print("500 tries to generate different individuals exceeded: GENERATED INDIVIDUAL IS THE SAME!");
                break

        j = 0
        for idx in range(0, len(mutVector),
                         3):  # check the mutations each three parameters - first is the function, sencond and third are the inputs
            initIdx = int(
                idx / 3) * self.numParamsNode  # since the individual vector is contiguous - this calculation is needed
            if mutVector[idx]:
                # mutate functions
                mutInd[initIdx] = rng.randint(self.getNumFunctions())

            for i in range(2):  # input0 and input1
                if mutVector[idx + i + 1]:  # check if input needs to be mutated - mutVector[idx+1] and mutVector[idx+2]
                    # mutate node layer
                    mutInd[initIdx + 2 + i * 2] = rng.randint(-1, j)  # mutInd[InitIdx+2] and mutInd[InitIdx+4]

                    # mutate node row
                    topLimit = self.numNodes
                    if mutInd[initIdx + 2 + i * 2] == -1:
                        topLimit = self.numInputs
                    mutInd[initIdx + 1 + i * 2] = rng.randint(0, topLimit)  # mutInd[InitIdx+1] and mutInd[InitIdx+3]

            # changeLayer if time has come
            j = int(idx / (self.numNodes * self.numParamsNode))

        return mutInd.astype(int)

    def evaluateIndividual(self, indexInd, ind, indexesMiniBatch):
        cumError = 0.0
        p1 = 0

        for idx in indexesMiniBatch:
            circuitSize, output = self.getOutput(ind, idx)
            p1 += 1 if output else 0
            cumError += 1 if output ^ self.dataset[idx][
                self.numInputs] else 0  # xor between target and output (error if they are different)

        p1 = p1 / len(indexesMiniBatch)
        acc = 1.0 - cumError / len(indexesMiniBatch)
        fit = acc

        if (
        self.useProbFit):  # consider also the probability distribution for the model and the dataset - this helps to avoid models with constant outputs
            fit = acc - 2 * abs(p1 - self.p1dataset)

        return fit, indexInd, circuitSize

    def getOutput(self, ind, indDataset, prt=False):
        self.listActNodes = {}
        cnt, val = self.getNodeOutput(ind, indDataset, 0, self.numLayers, prt)
        if self.useFunctionalSize:
            cnt = len(set(self.listActNodes.keys()))
        return cnt, val

    def getNodeOutput(self, ind, indDataset, i, j, prt=False):
        if not (i,j) in self.listActNodes.keys():
            self.listActNodes[i,j]=0
        self.listActNodes[i, j]+=1
        if j == -1:
            # print("Acessing index %i from the dataset and the input %i"%(indDataset,i))
            return 0, self.dataset[indDataset][i];
        else:
            node = self.getNode(ind, i, j)
            if prt:
                print("Node %i,%i" % (i, j))
                print(" ====> ", node)
            cnt1, val1 = self.getNodeOutput(ind, indDataset, node[1], node[2], prt)
            cnt2, val2 = self.getNodeOutput(ind, indDataset, node[3], node[4], prt)
            # don't sum if
            # print("Activating node %i,%i - actual count %i"%(i,j,(cnt1+cnt2+1)))
            return (cnt1 + cnt2 + 1), self.getFunction(node[0])(val1, val2)

    def getNode(self, ind, i, j):  # simple math to access i, j in a vector
        initInd = self.numNodes * self.numParamsNode * j + i * self.numParamsNode;
        endInd = initInd + self.numParamsNode
        # print("Size ind: %i accessing values from %i to %i"%(len(ind),initInd,endInd))
        # print(ind[initInd:endInd])
        return ind[initInd:endInd]

    def evolve(self):
        np.random.seed(self.seed)
        # fucntion needed to multiprocessing
        def worker(input, output):
            for func, args in iter(input.get, 'STOP'):
                try:
                    result, index, circuitSize = func.evaluateIndividual(args[0], args[1], args[2])
                except Exception as e:
                    print(e)
                output.put([result, args[0], circuitSize])

        # Create queues
        task_queue = Queue()
        done_queue = Queue()
        procs = []

        for i in range(self.nThreads):
            proc = Process(target=worker, args=(task_queue, done_queue))
            procs.append(proc)
            proc.start()

        # create random population
        self.createRandomPop()

        # create fitness statistics and circuit size vector for each generation
        self.statistics = np.zeros([self.nGen, 3])  # max avg and min population
        self.circuitSize = np.zeros([self.nGen])

        indexesBatch = []

        # auxiliary vectors for each generation
        fitness = np.zeros(self.popSize + self.popSize * self.nOffspring);
        circuitSize = np.zeros(self.popSize + self.popSize * self.nOffspring);
        # variable for defining when a parent has to be reevaluated
        reeval = True

        changeBatch=True
        if self.useAdaFitStoch:
            self.fitStoch = self.fitStochInit
        else:
            self.fitStoch = 0.001
        for nGen in range(self.nGen):
            start_time = time.time()
            offspring = []

            # when the batches have to be reevaluated
            if nGen % self.changeEach == 0 or changeBatch:
                changeBatch=False
                reeval = True
                l=list(range(len(self.dataset)))
                rng.shuffle(l)
                indexesBatch = l[:self.miniBatchSize]
                #indexesBatch = rng.randint(len(self.dataset),size=self.miniBatchSize)
                print("Changing batch Gen ", nGen)

            # evaluate all individuals and create their offspring
            numEval = 0
            for idx, ind in enumerate(self.pop):
                if nGen == 0 or self.reevalParent or reeval:  # evaluate the parents only in this condition
                    reeval = False
                    numEval += 1;
                    task_queue.put(((self, (idx, ind, indexesBatch))))
                for i in range(self.nOffspring):  # create offsprings
                    offspring.append(self.createIndividual(ind))
                    task_queue.put((self, (self.nOffspring * idx + i + self.popSize, offspring[-1], indexesBatch)))
                    numEval += 1;

            for i in range(numEval):  # get results
                results = done_queue.get();
                fitness[results[1]] = results[0]
                circuitSize[results[1]] = results[2]
                #print("Individual %i - Fitness %f - CircuitSize %i"%(results[1],results[0],results[2]))

            #adjust mutation rate based on the offspring generated
            for idOffspring in range(self.nOffspring):
                # adjust mutation rate - 1/5 rule - see https://arxiv.org/pdf/1810.09485.pdf
                if fitness[idOffspring+1] >= fitness[0]:  # child better than the parent
                    self.mutRate *= self.mutRateIncreaseBy #1.4
                else:  # parent selected
                    self.mutRate *= 1.4 ** (-1 / 4) #1.4
                    if self.mutRate < self.minMutRate:
                        self.mutRate = self.minMutRate

            # select new population
            allInd = np.concatenate((self.pop, offspring))
            bestInd = np.argsort(-fitness)

            # IMPORTANT!!!! WITH THIS MODIFICATION IT DOESN'T WORK WITH THE POPULATIONAL VERSION ANYMORE
            # check if there are individuals with the same fitness as the best, and choose the one with the biggest circuit
            best = bestInd[0]
            for i in range(1, len(bestInd)):
                if np.abs(fitness[best] - fitness[bestInd[i]]) <= self.fitStoch:  # use a small margin to not compare float numbers and increase the pressure for bigger circuits
                    if circuitSize[best] <= circuitSize[bestInd[i]]:  # if less than or equal to is used the mutation rate is constantly increased, try only less than to avoid this effect
                        best = bestInd[i]
                else:
                    break;

            #update stochasticity
            if self.useAdaFitStoch:
                self.fitStoch = (1-nGen/self.nGen)*self.fitStochInit
                if self.fitStoch < self.fitStochEnd:
                    self.fitStoch=self.fitStochEnd
            #if fitness[best]>=0.92:
                #self.mutRate=0.015
                #self.fitStoch=0.001

            # POP_SIZE NEEDS TO BE ALWAYS 1 AFTER THE LAST MODIFICATION
            # save statistics
            self.circuitSize[nGen] = circuitSize[best]
            self.statistics[nGen] = [fitness[best], fitness[best], fitness[best]]

            self.pop[0] = allInd[best]  # select the best individual
            fitness[0] = fitness[best]  # fill its fitness
            circuitSize[0] = circuitSize[best]  # fill its circuitSize

            # do some printing
            if (nGen%5)==0:
                # some debug info if needed
                # print([{"fit": "%.3f" % (data[0]), "size": data[1]} for data in zip(fitness, circuitSize)])
                print("Seed %i - Generation %i mutRate %f - Best = %f (%i) CircuitSize = %i - BatchSize %i --- Time to run: %.2f seconds ---" % (self.seed, nGen, self.mutRate, self.statistics[nGen][0], best, circuitSize[best], self.miniBatchSize,(time.time() - start_time)))

            if (abs(fitness[best] - 1) < self.solutionThreshold):
                self.bestFit=fitness[best]
                print("=================== SOLUTION FOUND AT GEN %i==================="%(nGen))
                if (self.miniBatchSize == len(self.dataset)):
                    self.solutionFound=True
                    #fit, _, _ = self.evaluateIndividual(0, self.pop[0], range(len(self.dataset)))
                    #print("Fitness BEST INSIDE CGP: ", fitness[best])
                    #print("Best individual is",best)
                    #print("Fitness on the whole dataset INSIDE CGP: ", fit)
                    break;
                else:
                    changeBatch = True

        if self.saveResults:
            nameDir=self.getResultsNameDir()
            np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%i.fit" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed), self.statistics)
            np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%i.pop" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed), self.pop)
            np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%i.size" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed), self.circuitSize)
        # Tell child processes to stop
        for i in range(self.nThreads):
            task_queue.put('STOP')

    def getResultsNameDir(self):
        nameDir=""

        if self.aws:
            nameDir="aws1/"

        if self.useProbFit:
            nameDir+="probFit"
        else:
            nameDir+="notProbFit"

        nameDir+="-"+str(self.mutRateIncreaseBy)+"-replEqual"

        if self.useAdaFitStoch:
            nameDir+="-AdaStoch"

        if self.useFunctionalSize:
            nameDir+="-FuncSize"

        os.makedirs("results/"+nameDir,exist_ok=True)

        return nameDir

    def printInd(self, ind):  # print all nodes of an individual
        for i in range(0, len(ind), 6):
            print("Node %i,%i: i1 = (%i,%i) iv = %i --- i2= (%i,%i) iv2 = %i" % (
            0, int(i / 6), indXor[i], indXor[i + 1], indXor[i + 2], indXor[i + 3], indXor[i + 4], indXor[i + 5]))

    def printFuncInd():  # print only the functional part of an individual
        self.getOutput(cgp.pop[0], self.dataset[0], True)