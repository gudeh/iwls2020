import numpy as np
from matplotlib import pyplot as plt
import os
from cgp2 import CGP
import re
from collections import defaultdict,OrderedDict

def extractParams(fileName):
    m=re.search("ex([0-9]+)Bat([0-9]+)ChEach([0-9]+)Nod([0-9]+)Lay([0-9]+)S([0-9]+)",fileName)
    example=m.group(1);batchSize=m.group(2);changeEach=m.group(3);numRows=m.group(4);numLayers=m.group(5);seed=m.group(6)
    return example, int(batchSize), int(changeEach), int(numRows), int(numLayers), int(seed)

fileResume="results/cgp2-sag/notProbFit-1.05-replEqual-FuncSize-resetMR300/ex02Bat1000ChEach2000Nod1Lay500S3.pop.npy"
example, batchSize, changeEach, numRows, numLayers, seed=extractParams(fileResume)

nGen=200000
#batchSize=1000
#changeEach=2000


data = np.genfromtxt("data/ex%s.valid.csv"%(example),delimiter=",");
cgp = CGP(numRows,numLayers,1,data,999);
cgp.pop = np.load(fileResume)
cgp.nGen = nGen
cgp.initMutRate=0.1
cgp.minMutRate=0.0001
cgp.resetMutOnChangeBatch=True
cgp.keepMutRateFor=int(changeEach*0.15)
cgp.nThreads=1
cgp.miniBatchSize=batchSize
cgp.changeEach=changeEach
cgp.example=example
cgp.evolve(True)