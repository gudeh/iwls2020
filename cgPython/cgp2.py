import numpy as np
from numpy import random as rng
import time
import os

np.set_printoptions(threshold=29406,linewidth=85)

class CGP:
    def __init__(self, numNodes, numLayers, popSize, dataMat, seed=99):
        self.numNodes = numNodes
        self.numLayers = numLayers
        self.numTotNodes = numNodes * numLayers
        self.popSize = popSize
        self.fitnessPop = np.zeros([popSize, 1])
        self.p1dataset = dataMat.sum(axis=0)[-1] / len(dataMat)
        self.dataset = dataMat == 1
        self.numInputs = dataMat.shape[1] - 1
        self.numParamsNode = 7  # func i1 j1 iv1 i2 j2 iv2
        self.numParams = numLayers * numNodes * self.numParamsNode + 3 # last three parameters indicate which is the output node and an inversion bit
        self.backlevel = numLayers #not implemented - should indicate how many layers behind each node can connect
        self.pop = np.zeros([self.popSize, self.numParams], dtype=int)
        self.solutionFound=False
        self.replaceWhenCSizeEqual=False

        # evolution params
        self.nGen = 10000
        self.nOffspring = 4
        self.resetMutOnChangeBatch = False
        self.keepMutRateFor = 0
        self.initMutRate = 1
        self.minMutRate = 0.001
        self.miniBatchSize = 100
        self.reevalParent = False
        self.changeEach = 500
        self.nBatchSolutionFound=5
        self.useProbFit = False
        self.useAllFunctions = True  # set to false to use only AND functions
        self.useFunctionalSize = True #consider the circuit size as the number of unique nodes used (True) x the total number of nodes (False)
        self.saveResults=True

        self.numFuncNodes=0
        self.listActNodes={}
        self.aws=True
        self.sag=False

        self.useAdaFitStoch = False
        self.fitStochInit=0.03
        self.fitStochEnd = 0.001

        self.bestFit = 0
        self.genBestFit=0
        self.solutionThreshold = 0.0001

        self.mutRateIncreaseBy=1.05

        self.seed = seed
        np.random.seed(self.seed)
        # create functions
        self.functions = []

        self.setUseAllFunctions(self.useAllFunctions)

    def createRandomPop(self):
        self.setInitMutRate(self.initMutRate)
        for i in range(self.popSize):
            self.pop[i] = self.createIndividual()

    # return a  new or a mutated copy of an individual
    def createIndividual(self, ind=None):
        if ind is None:  # if no individual is provided create a new one - by randomly generating all nodes
            mutInd = np.zeros(self.numParams)
            mutVector = np.zeros(self.numTotNodes * 5+2)  # 3 because it is input1 iv1 input2 iv2 and function + 1 which is the output param
        else:
            mutInd = ind.copy()
            # implement mutaiton using percentage
            mutVector = rng.rand(self.numTotNodes * 5+2)  # randonly draw numbers from a uniform distribution to define which nodes will be replaced

        mutVector = mutVector < self.mutRate  # Create a boolean vector of the nodes to replace based on the mutation rate
        cnt = 0
        if np.sum(mutVector) == 0:  # be sure that at least one node will be mutated
            indToChange=rng.randint(len(mutVector))
            mutVector[indToChange] = True
            #print("===== No mutations generated - forcing a mutation on index ",indToChange)

        j = 0
        for idx in range(0, len(mutVector)-2, 5):  # check the mutations each five parameters - first is the function, second and third are the inputs and inversions - keep the last two out
            initIdx = int(idx/5) * self.numParamsNode  # since the individual vector is contiguous - this calculation is needed
            if mutVector[idx]: #if first value is true it means we need to mutate the function
                # mutate functions
                mutInd[initIdx] = rng.randint(self.getNumFunctions())

            for i in range(2):  # input0 and input1
                if mutVector[idx + i*2 + 1]:  # check if input needs to be mutated - mutVector[idx+1] and mutVector[idx+2]
                    # mutate node layer
                    mutInd[initIdx + 2 + i * 3] = rng.randint(-1, j)  # mutInd[InitIdx+2] and mutInd[InitIdx+4]

                    # mutate node row
                    topLimit = self.numNodes
                    if mutInd[initIdx + 2 + i * 3] == -1:
                        topLimit = self.numInputs
                    mutInd[initIdx + 1 + i * 3] = rng.randint(0, topLimit)  # mutInd[InitIdx+1] and mutInd[InitIdx+3]

                if mutVector[idx + i*2 + 2]: #this means we need to mutate the inversion bit
                    mutInd[initIdx + 3 + i*3]= rng.randint(2) #0 or 1

            # changeLayer if time has come
            j = int(idx / (self.numNodes * self.numParamsNode))

        #now check if the last param (output node) should be mutated - node and inversion separately
        if mutVector[-2]: #mutate the output node
            mutInd[-3] = rng.randint(self.numNodes)
            mutInd[-2] = rng.randint(self.numLayers)

        if mutVector[-1]: #mutate the inversion of the output
            mutInd[-1] = rng.randint(2)

        return mutInd.astype(int)

    def setInitMutRate(self,f):
        self.initMutRate=f
        self.mutRate=f

    def setUseAllFunctions(self, bool=True):
        self.useAllFunctions = bool
        self.numFunctions = 6 if self.useAllFunctions else 2

    def AND(self, x, y):
        return x & y

    def NAND(self, x, y):
        return not (x & y)

    def OR(self, x, y):
        return x | y

    def NOR(self, x, y):
        return not (x | y)

    def XOR(self, x, y):
        return x ^ y

    def NXOR(self, x, y):
        return not (x ^ y)

    def getFunction(self, ind):  # unfortunately lambda functions cannot be used because the multiprocessing lib - they can't be pickled
        if ind == 0:
            return self.AND
        elif ind == 1:
            return self.NAND
        elif ind == 2:
            return self.OR
        elif ind == 3:
            return self.NOR
        elif ind == 4:
            return self.XOR
        elif ind == 5:
            return self.NXOR

    def getNumFunctions(self):
        return self.numFunctions

    def evaluateIndividual(self, indexInd, ind, indexesMiniBatch):
        cumError = 0.0
        p1 = 0

        for idx in indexesMiniBatch:
            circuitSize, output = self.getOutput(ind, idx)
            p1 += 1 if output else 0
            cumError += 1 if output ^ self.dataset[idx][
                self.numInputs] else 0  # xor between target and output (error if they are different)

        p1 = p1 / len(indexesMiniBatch)
        acc = 1.0 - cumError / len(indexesMiniBatch)
        fit = acc

        if (
        self.useProbFit):  # consider also the probability distribution for the model and the dataset - this helps to avoid models with constant outputs
            fit = acc - 2 * abs(p1 - self.p1dataset)

        return fit, indexInd, circuitSize

    def getOutput(self, ind, indDataset, prt=False):
        self.listActNodes = {}
        cnt, val = self.getNodeOutput(ind, indDataset, ind[-3], ind[-2], prt)
        if self.useFunctionalSize:
            #cnt = len(set(filter(lambda x: x[1]>=0,set(self.listActNodes.keys())))) #this is for NOT considering inputs as size
            cnt = len(set(self.listActNodes.keys()))  # this is for considering inputs as size
            if prt:
                print("Unique activated nodes = ",set(filter(lambda x: x[1]>=0,set(self.listActNodes.keys()))))
                print("Number Activated nodes = ",cnt)
        return cnt, np.abs(val-ind[-1])

    def getNodeOutput(self, ind, indDataset, i, j, prt=False):
        if not (i,j) in self.listActNodes.keys():
            self.listActNodes[i,j]=0
        self.listActNodes[i, j]+=1

        if j == -1:
            # print("Acessing index %i from the dataset and the input %i"%(indDataset,i))
            return 0, self.dataset[indDataset][i];
        else:
            initInd = self.numNodes * self.numParamsNode * j + i * self.numParamsNode;
            endInd = initInd + self.numParamsNode
            node = ind[initInd:endInd]  #0=function 1 and 2=input1 3=inv1 4 and 5=input2 6=inv2
            if prt:
                print("Node %i,%i" % (i, j))
                print(" ====> ", node)
            cnt1, val1 = self.getNodeOutput(ind, indDataset, node[1], node[2], prt)
            cnt2, val2 = self.getNodeOutput(ind, indDataset, node[4], node[5], prt)
            # don't sum if
            # print("Activating node %i,%i - actual count %i"%(i,j,(cnt1+cnt2+1)))
            func = [self.AND,self.NAND,self.OR,self.NOR,self.XOR,self.NXOR]
            return (cnt1 + cnt2 + 1), func[node[0]](np.abs(val1-node[3]), np.abs(val2-node[6]))

    def getNode(self, ind, i, j):  # simple math to access i, j in a vector
        initInd = self.numNodes * self.numParamsNode * j + i * self.numParamsNode;
        endInd = initInd + self.numParamsNode
        # print("Size ind: %i accessing values from %i to %i"%(len(ind),initInd,endInd))
        # print(ind[initInd:endInd])
        return ind[initInd:endInd]

    def evolve(self,startFromPop=None):
        # create random population
        if startFromPop is None:
            self.createRandomPop()

        # create fitness statistics and circuit size vector for each generation
        self.statistics = np.zeros([self.nGen, 3])  # max avg and min population
        self.circuitSize = np.zeros([self.nGen])

        indexesBatch = []

        # auxiliary vectors for each generation
        fitness = np.zeros(self.popSize + self.popSize * self.nOffspring);
        circuitSize = np.zeros(self.popSize + self.popSize * self.nOffspring);
        # variable for defining when a parent has to be reevaluated
        reeval = True

        keepMutCount=0
        solutionFoundCount=0
        changeBatch=True
        self.mutRate = self.initMutRate

        if self.useAdaFitStoch:
            self.fitStoch = self.fitStochInit
        else:
            self.fitStoch = 0.00001

        for nGen in range(self.nGen):
            start_time = time.time()
            offspring = []

            # when the batches have to be changed
            if nGen % self.changeEach == 0 or changeBatch:
                #print("PARENT: %s"%(self.pop[0]))
                if self.resetMutOnChangeBatch:
                    self.mutRate = self.initMutRate
                keepMutCount=0
                changeBatch=False
                reeval = True
                l=list(range(len(self.dataset)))
                rng.shuffle(l)
                indexesBatch = l[:self.miniBatchSize]
                #indexesBatch = rng.randint(len(self.dataset),size=self.miniBatchSize)
                print("Changing batch Gen ", nGen)

            # evaluate all individuals and create their offspring
            for idx, ind in enumerate(self.pop):
                if self.reevalParent or reeval:  # evaluate the parents only in this condition
                    print("------------------------------------ EVALUATING PARENT ------------------------------------")
                    reeval = False
                    #startTime=time.time()
                    fitness[idx], _, circuitSize[idx] = self.evaluateIndividual(idx, ind, indexesBatch)
                    #print("Time for parent (cSize %i) evaluation %.3f"%(circuitSize[idx],time.time() - startTime))

                for i in range(self.nOffspring):  # create offsprings
                    idOffs=self.nOffspring * idx + i + self.popSize
                    startTime = time.time()
                    offspring.append(self.createIndividual(ind))
                    #creationTime=time.time() - startTime
                    #startTime = time.time()
                    fitness[idOffs], _, circuitSize[idOffs] = self.evaluateIndividual(idOffs, offspring[-1], indexesBatch)
                    #print("Time for offspring %i (cSize %i) creation %.3f evaluation %.3f" % (i,circuitSize[idOffs],creationTime, (time.time() - startTime)))

            #print("PARENT: %s"%(self.pop[0]))
            #adjust mutation rate based on the offspring generated
            if keepMutCount>=self.keepMutRateFor:
                for idOffspring in range(self.nOffspring):
                    # adjust mutation rate - 1/5 rule - see https://arxiv.org/pdf/1810.09485.pdf
                    if fitness[idOffspring+1] >= fitness[0]:  # child better than the parent
                        self.mutRate *= self.mutRateIncreaseBy #1.4
                    else:  # parent selected
                        self.mutRate *= 1.4 ** (-1 / 4) #1.4
                        if self.mutRate < self.minMutRate:
                            self.mutRate = self.minMutRate
            else:
                keepMutCount+=1

            # select new population
            allInd = np.concatenate((self.pop, offspring))
            bestInd = np.argsort(-fitness)

            # IMPORTANT!!!! WITH THIS MODIFICATION IT DOESN'T WORK WITH THE POPULATIONAL VERSION ANYMORE
            # check if there are individuals with the same fitness as the best, and choose the one with the biggest circuit
            best = bestInd[0]
            for i in range(1, len(allInd)):
                if np.abs(fitness[best] - fitness[bestInd[i]]) <= self.fitStoch:  # use a small margin to not compare float numbers and increase the pressure for bigger circuits
                    if circuitSize[best] <= circuitSize[bestInd[i]]:  # if less than or equal to is used the mutation rate is constantly increased, try only less than to avoid this effect
                        best = bestInd[i]

            #update stochasticity
            if self.useAdaFitStoch:
                self.fitStoch = (1-nGen/self.nGen)*self.fitStochInit
                if self.fitStoch < self.fitStochEnd:
                    self.fitStoch=self.fitStochEnd
            #if fitness[best]>=0.92:
                #self.mutRate=0.015
                #self.fitStoch=0.001

            # some debug info if needed
            #print([{"fit": "%.3f" % (data[0]), "size": data[1]} for data in zip(fitness, circuitSize)])

            # POP_SIZE NEEDS TO BE ALWAYS 1 AFTER THE LAST MODIFICATION
            # save statistics
            self.circuitSize[nGen] = circuitSize[best]
            self.statistics[nGen] = [fitness[best], fitness[best], fitness[best]]

            # do some printing
            if (nGen%1)==0:
                print(
                    "Seed %i - Generation %i mutRate %f - Best = %f (%i) CircuitSize = %i - BatchSize %i --- Time to run: %.2f seconds ---" % (
                    self.seed, nGen, self.mutRate, self.statistics[nGen][0], best, circuitSize[best],
                    self.miniBatchSize, (time.time() - start_time)))

                #print([{"fit": "%.3f" % (data[0]), "size": data[1]} for data in zip(fitness, circuitSize)])
                #print("PARENT: %s" % (self.pop[0]))
                #self.printInd(self.pop[0])
                #print("Dataset length=%i - inputs %i"%(len(self.dataset),len(self.dataset[0])))
                #print("+++++++++ PARENT +++++++++++ fit %f cSize %i Signature %i +++++++++++"%(fitness[0],circuitSize[0],np.sum(self.pop[0])))
                #self.printInd(self.pop[0])
                #self.getOutput(self.pop[0],0,True)
                #fit, _, cSize = self.evaluateIndividual(0, self.pop[0], indexesBatch)
                #print("+++++++++========= NEW EVAL PARENT fit %f cSize %i Signature %i +++++++++++" % (fit, cSize,np.sum(self.pop[0])))

                #for offs in range(len(offspring)):
                #    print("+++++++++ OFFSPRING %i fit %f cSize %i Signature %i +++++++++++"%(offs,fitness[offs+1],circuitSize[offs+1],np.sum(offspring[offs])))
                    #self.printInd(offspring[offs])
                #    self.getOutput(offspring[offs], 0, True)
                #    fit,_,cSize=self.evaluateIndividual(offs, offspring[offs], indexesBatch)
                #   print("+++++++++========= NEW EVAL OFFSPRING %i fit %f cSize %i Signature %i +++++++++++" % (offs, fit, cSize,np.sum(offspring[offs])))

            #print("INDIVIDUAL CHOSEN ",best)

            #fit, _, cSize = self.evaluateIndividual(0, self.pop[0], indexesBatch)
            #self.getOutput(self.pop[0], 0, True)
            #print("+++++++++========= NEW EVAL PARENT fit %f cSize %i +++++++++++" % (fit, cSize))
            #for i in range(len(allInd)):
            #    print("============ Allind ",i)
            #    fit, _, cSize = self.evaluateIndividual(0, allInd[i], indexesBatch)
            #    self.getOutput(allInd[i], 0, True)
            #    print("+++++++++========= NEW EVAL ALL IND fit %f cSize %i +++++++++++" % (fit, cSize))

            #print("BEST (allInd %i) signature is  %i"%(best,np.sum(allInd[best])))
            self.pop[0] = np.array(allInd[best])  # select the best individual
            #print("Pop signature is now ",np.sum(self.pop[0]))
            fitness[0] = fitness[best]  # fill its fitness
            circuitSize[0] = circuitSize[best]  # fill its circuitSize
            self.bestFit = fitness[best]
            self.genBestFit = nGen

            if (abs(fitness[best] - 1) < self.solutionThreshold):
                solutionFoundCount+=1
                print("=================== SOLUTION FOUND ===================")
                print("Node output = (%i,%i) inv=%i"%(self.pop[0][-3],self.pop[0][-2],self.pop[0][-1]))
                self.getOutput(self.pop[0], 0, True)
                fit, _, cSize = self.evaluateIndividual(0, self.pop[0], indexesBatch)
                print("+++++++++========= NEW EVAL SOLUTION fit %f cSize %i Signature %i +++++++++++" % (
                fit, cSize, np.sum(self.pop[0])))
                if self.miniBatchSize == len(self.dataset) or solutionFoundCount>=self.nBatchSolutionFound:
                    self.solutionFound=True
                    #fit, _, _ = self.evaluateIndividual(0, self.pop[0], range(len(self.dataset)))
                    #print("Fitness BEST INSIDE CGP: ", fitness[best])
                    #print("Best individual is",best)
                    #print("Fitness on the whole dataset INSIDE CGP: ", fit)
                    break;
                else:
                    changeBatch = True
            else:
                solutionFoundCount=0

            if self.saveResults and (nGen%10000)==0:
               nameDir=self.getResultsNameDir()
               np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%iG%i.fit" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed,nGen), self.statistics)
               np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%iG%i.pop" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed,nGen), self.pop)
               np.save("results/"+nameDir+"/ex%sBat%iChEach%iNod%iLay%iS%iG%i.size" % (str(self.example).zfill(2), self.miniBatchSize, self.changeEach, self.numNodes, self.numLayers, self.seed,nGen), self.circuitSize)


    def getResultsNameDir(self):
        nameDir="cgp2-"

        if self.aws:
            nameDir+="aws/"
        elif self.sag:
            nameDir+="sag/"

        if self.useProbFit:
            nameDir+="probFit"
        else:
            nameDir+="notProbFit"

        nameDir+="-"+str(self.mutRateIncreaseBy)+"-replEqual"

        if self.useAdaFitStoch:
            nameDir+="-AdaStoch"

        if self.useFunctionalSize:
            nameDir+="-FuncSize"

        if self.resetMutOnChangeBatch:
            nameDir+="-noResetMR"
        else:
            nameDir += "-resetMR"+str(self.keepMutRateFor)

        os.makedirs("results/"+nameDir,exist_ok=True)

        return nameDir

    def printInd(self, ind):  # print all nodes of an individual
        for i in range(0, len(ind)-3, 7):
            print("Node %i,%i: f = %i; i1 = (%i,%i) iv = %i --- i2= (%i,%i) iv2 = %i" % (
            0, int(i / 7), ind[i], ind[i + 1], ind[i + 2], ind[i + 3], ind[i + 4], ind[i + 5], ind[i+6]))
        print('==> Output %i %i %i'%(ind[-3],ind[-2],ind[-1]))

    def printFuncInd():  # print only the functional part of an individual
        self.getOutput(self.pop[0], self.dataset[0], True)
