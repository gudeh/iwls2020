import pandas as pd
import numpy as np
from cgp import CGP
from graphviz import Digraph
import uuid


class TreeNode:

    def __init__(self, df, parent=None):
        self.nodeId=uuid.uuid1()
        self.parent = parent
        self.data = df
        self.numInputs = df.shape[1] - 1 if self.parent is None else self.parent.numInputs
        self.indexes = None
        self.leftNode = None
        self.rightNode = None
        self.cgp = None

        print("Node created with %i instances and %i columns - total inputs is always %i "%(len(self.data),len(self.data.columns),self.numInputs))

        if parent is None:
            self.stopDivAt = 1800
        else:
            self.stopDivAt = self.parent.stopDivAt

        self.cgpLayers = 20
        self.cgpNumGen = 5000
        self.cgpNumSeeds = 10
        self.cgpBest = None

    def reduceAndDivide(self):
        sameValue = np.zeros([self.numInputs, self.numInputs])

        for idCol1 in self.data.columns.values[:-1]:
            for idCol2 in self.data.columns.values[:-1]:
                sameValue[idCol1][idCol2] = 0 if idCol1 == idCol2 else sum(self.data[idCol1] == self.data[idCol2])

        ind1 = np.argmax(np.max(sameValue, axis=0))
        ind2 = np.argmax(sameValue[ind1])

        df1,df2 = self.divideDataByIndex(self.data,ind1,ind2) #df1 is reduced by one dimension

        self.indexes = (ind1, ind2)
        print("Dataset divided by same values on ",self.indexes)
        self.leftNode = TreeNode(df1, self)
        self.leftNode.solve()
        self.rightNode = TreeNode(df2, self)
        self.rightNode.solve()

    def divideDataByIndex(self,data,ind1,ind2):
        dividedDataLeft = data[data[ind1] == data[ind2]].drop([ind2], axis=1)
        dividedDataRight = data[data[ind1] != data[ind2]]
        return dividedDataLeft, dividedDataRight

    def solve(self):
        if len(self.data) < self.stopDivAt:
            if self.data.shape[0]>0:
                cgps = []
                best = 0;
                bestInd = -1;
                for seed in range(self.cgpNumSeeds):
                    cgp = CGP(1,self.cgpLayers,1,np.array(self.data.values.tolist()),seed)
                    cgp.nGen = self.cgpNumGen
                    cgp.miniBatchSize = len(self.data)
                    cgp.changeEach = self.cgpNumGen
                    self.replaceWhenCSizeEqual=True
                    cgp.saveResults = False
                    cgp.evolve()
                    fit, _, _ = cgp.evaluateIndividual(0, cgp.pop[0], range(len(cgp.dataset)))
                    print("Fitness on the whole dataset after evolution: ", fit)
                    cgps.append(cgp)
                    if not cgp.solutionFound:
                        if cgp.statistics[-1][0]>=best:
                            best=cgp.statistics[-1][0]
                            bestInd=seed
                        print("SOLUTION NOT FOUND - BEST SO FAR IS SEED %i FITNESS %f" % (bestInd, best))
                    else:
                        best=1
                        bestInd=seed
                        print("SOLUTION FOUND Seed %i - STOPING HERE: dataset size=%i"%(seed,self.data.shape[0]))
                        break
                self.cgp = cgps[bestInd]
                fit,_,_ = self.cgp.evaluateIndividual(0, self.cgp.pop[0], range(len(self.cgp.dataset)))
                print("Fitness on the whole dataset: ",fit)
                self.cgpBest=best
                #if not cgp.solutionFound:
                    #print("SOLUTION NOT FOUND  - BEST WAS SEED %i FITNESS %f - KEEP DIVIDING..." % (seed, best))
                    #self.reduceAndDivide()

            else:
                print("THIS DATASET (NODE) IS EMPTY")
        else:
            self.reduceAndDivide();

    def test(self, data):
        accLeft = 0
        accRight = 0
        if self.leftNode is None:
            self.cgp.dataset = np.array(data==1)
            fit,_,_ = self.cgp.evaluateIndividual(0, self.cgp.pop[0], range(len(self.cgp.dataset)))
            return fit
        else:
            dataLeft, dataRight = self.divideDataByIndex(data,self.indexes[0],self.indexes[1])
            accLeft=self.leftNode.test(dataLeft)
            if (not self.rightNode is None) and len(dataRight>0):
               accRight=self.rightNode.test(dataRight)
            return (accLeft+accRight)/2

    def test(self, data):
        accLeft = 0
        accRight = 0
        if self.leftNode is None:
            # print("Number of instances BEFORE: ", len(root.cgp.dataset))
            self.cgp.dataset = np.array(data.values.tolist()) == 1
            # print("Number of instances that will be tested: ", len(root.cgp.dataset))
            fit, _, _ = self.cgp.evaluateIndividual(0, self.cgp.pop[0], range(len(self.cgp.dataset)))
            # print("RESULT IS ",fit)
            print("Test done with %i instances - %f accuracy" % (len(self.cgp.dataset), fit))
            return len(self.cgp.dataset), fit
        else:
            count = 0
            numInstancesRight = 0
            numInstancesLeft = 0
            dataLeft, dataRight = self.divideDataByIndex(data, self.indexes[0], self.indexes[1])
            print("Data divided %i left %i right" % (len(dataLeft), len(dataRight)))

            if (not self.leftNode is None) and len(dataLeft > 0):
                numInstancesLeft, accLeft = self.leftNode.test(dataLeft)
                count += 1

            if (not self.rightNode is None) and len(dataRight > 0):
                numInstancesRight, accRight = self.rightNode.test(dataRight)
                count += 1

            return (numInstancesLeft + numInstancesRight), (accLeft + accRight) / count