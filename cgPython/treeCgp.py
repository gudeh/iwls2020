import numpy as np
from matplotlib import pyplot as plt
import os
import re
from collections import defaultdict,OrderedDict
import pandas as pd
import pickle
from TreeNode import TreeNode

examplesToTrain = [21,24,25,40,45,47,59,74,84,85]

#5 - 500
#6 - 6400 ex01
#7 - 6400 ex00
#8 - 6400 ex00 - stopDivAt 800
#9 - 6400 ex01 - 1800 1800

example=1
for example in examplesToTrain:
    data = np.genfromtxt("data/ex%s.train.csv"%(str(example).zfill(2)),delimiter=",");
    df=pd.DataFrame(data)
    root = TreeNode(df)
    root.stopDivAt = 800
    root.solve()

    with open('results/TreeCgpExample%sDiv%iLay%i.obj'%(str(example).zfill(2),root.stopDivAt,root.cgpLayers), 'wb') as f:
        pickle.dump(root, f)