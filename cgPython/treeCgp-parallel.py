import numpy as np
import pandas as pd
import pickle
from TreeNodeP import TreeNode
import os

examplesToTrain = [0,1,21,24,25,40,45,47,51,59,74,84,85]
#examplesToTrain = [24 51]
divLevel=3 #1=3600 (aprox. 2 trees) - 2=2000 (4 trees) 3=1100 (8 trees) 4=600 (16 trees) 5=300 (32 trees) 6=200 (64 trees) 7=100 (128 trees) - number of trees is an estimate
numLayers = 20
numGen = 5000
numSeeds = 3

#5 - 500
#6 - 6400 ex01
#7 - 6400 ex00
#8 - 6400 ex00 - stopDivAt 800
#9 - 6400 ex01 - 1800 1800


div = {1:3600,2:2000,3:1100,4:600,5:300,6:200,7:100}

totalCpus = os.cpu_count()
estNumTrees=2**divLevel
numProcTree=estNumTrees if estNumTrees <= totalCpus else totalCpus
vCpu=int(totalCpus/estNumTrees)
numProcSeed=vCpu if vCpu<=numSeeds else numSeeds
numProcSeed=1 if numProcSeed<1 else numProcSeed

print("%i cgps will be evolved in parallel."%(numProcTree))
print("%i seeds will be evolved in parallel for each cgp."%(numProcSeed))
print("In total %i processes will run in parallel"%(numProcTree*numProcSeed))

if numProcTree*numProcSeed<totalCpus:
    numProcTree+=totalCpus-numProcTree*numProcSeed

for example in examplesToTrain:
    data = np.genfromtxt("data/ex%s.train.csv"%(str(example).zfill(2)),delimiter=",");
    df=pd.DataFrame(data)
    root = TreeNode(df)
    root.numProcTree=numProcTree
    root.numProcSeed=numProcSeed
    root.stopDivAt = div[divLevel]
    root.cgpLayers = numLayers
    root.cgpNumGen = numGen
    root.cgpNumSeeds = numSeeds
    root.solve()
    root.trainLeaves()

    nodes = []

    with open('results/tree/TreeCgpParallelExample%sDiv%iLay%inGen%inSeed%i.obj'%(str(example).zfill(2),root.stopDivAt,root.cgpLayers,numGen,numSeeds), 'wb') as f:
        pickle.dump(root, f)