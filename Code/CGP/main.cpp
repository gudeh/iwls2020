/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on April 3, 2020, 1:41 PM
 */

#include "CGP.h"
#include "AIG.h"
#include "binaryPLA.h"
#include <algorithm>
#include <chrono>  // for high_resolution_clock
#include <unistd.h> //sleep
#if run_option == 2
#include <filesystem> //requires c++17
namespace fs = std::filesystem;
#endif



using namespace std;
struct write_struct{
    int temp,num_ands,generation,originalPis,functionalPis;
    float best,mutation;
} generation_info;
vector<write_struct> record(write_size);
int write_offset=0;

int main(int argc, char** argv) {
    system("rm -rf ../../AIGs/*.aig");
    system("rm -rf ../../Learning/*.csv");
    auto begin_all = std::chrono::high_resolution_clock::now(); //start measuring time from here
    std::chrono::duration<double> elapsed; //variable to measure time
    binaryPLA whole_pla,valid_pla;
    string pla_train_exten,pla_valid_exten,pla_test_exten,exemplar_name,init_type="Rand";
    int column_size,line_size=1,PIs,change_each=0,pla_batch_size,adaptative_change_interval=2000,
        complete_batch_size,num_individuals=1,cgp_size=0, init_batch_value;
    double min_mutation=0.01,mutation_chance,init_mut=2;   
    float partial_score=0, batch_change_percentage;
    tuple<float,float,float> scores_pair;
    tuple<int,int,float> out_abc;
    random_device device;
//    mt19937 mt(seed_num);
    
//    system("rm Learning/fitnes*.csv");
    

    ofstream results("../../results.csv", ios::out),learning_fitnes; //,sbcci("sbcci.csv",ios::out), //variables to write learning info
//    sbcci<<"original size,original level,original score,new size,new level,new score"<<endl;
    results<<"Init,Seed,CGPSize,ChangeEach,Name,TrainAcc,ValidAcc,TestAcc1,TestAcc2,ANDsFunc,LogicLevel,PIsOriginal,PIsFunc,Time,BatchSize,FullName,Generations,ChangePercentage,AdaptativeInterval"<<endl;
    results.close();
    CgpPopulation my_CgpPopulation;
    AigPopulation aig_popu_obj;
    
    
    string learning_file_name="../../Learning/fitnes-ex00.csv";
    int seed_init=0,seed_end=9,max_no_gain=5000,num_generations=50000, finished_gen=num_generations;
    exemplar_name="ex00";
   vector<int> exemplars={5};  //vector with exemplar numbers to be processed;
    // vector<int> exemplars={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
    //                        31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,
    //                        59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79}; //26,48,78 are constants
    //outer loop. Choosing exemplars from "exemplars" vector of int's
//    for(int s=0;s<exemplars.size();s++)
//    {
    #if run_option == 1
    for(int s=0;s<exemplars.size();s++)
    {
        int counter=exemplars[s]; 
    #elif run_option == 2
    string path = aigs_path;
    cout<<path<<endl;
    for (const auto & entry : fs::directory_iterator(path))
    {
        cout <<"entry:" << entry.path() << endl;
        string my_str=entry.path();
        cout<<my_str<<", size:"<<my_str.size()<<endl;
        my_str.erase(0,my_str.find_last_of("ex")+1);
        my_str.erase(2,my_str.size());
        int counter=atoi(my_str.c_str());
        cout<<"COUNTER++++++++"<<counter<<endl;
        
        my_str=entry.path();
        cout<<my_str<<endl;
        my_str.erase(my_str.find_last_of(".aig")-3,my_str.size());
        my_str.erase(0,my_str.find_last_of("/")+1);
        cout<<"FULL NAME+++++++"<<my_str<<endl;
        string full_name=my_str;
    #elif run_option == 3
        int counter=0,g=1;
        vector<int> exemplars_argv(argc-1);
        for (g=1;g<argc;g++)
            exemplars_argv[g-1]=atoi(argv[g])-1;
//        sort(exemplars_argv.begin(),exemplars_argv.end());
          sort(exemplars_argv.rbegin(),exemplars_argv.rend());
//        shuffle(exemplars_argv.begin(),exemplars_argv.end(),mt);
        cout<<"inputs:";
        for (g=1;g<argc;g++)
            cout<<exemplars_argv[g-1]<<","; cout<<endl;//=atoi(argv[g])-1;

        for(int s=0;s<exemplars_argv.size();s++)
        {
                counter=exemplars_argv[s];      
    #endif
            pla_train_exten=".train.pla";    //training data expected extension 
            pla_valid_exten=".valid.pla";
            pla_test_exten=".test.pla";
           
            //handling string name for the input PLA
            if(counter<=9)
                {exemplar_name.replace(3,1,to_string(counter)); exemplar_name.replace(2,1,to_string(0));}
            else
                exemplar_name.replace(2,2,to_string(counter)); 
            //handling string name for output csv
            if(counter<=9)
                learning_file_name.replace(25,1,to_string(counter));
            else
                learning_file_name.replace(24,2,to_string(counter));
            
            // if(counter==26 || counter==78 || counter==48)
            //     continue;
            cout<<learning_file_name<<endl;
            string pla_path_str=pla_path;
            string pla_train_filepath = pla_path_str+exemplar_name+pla_train_exten;
            complete_batch_size = whole_pla.getBatchSizeFile(pla_train_filepath);
            cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><"<<endl;
            cout<<">>>>>>>>PROCESSING "<<exemplar_name<<"<<<<<<<<<<<<<"<<endl;        
            cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><"<<endl;
            whole_pla.clear();
            // cout<<"EXEM{:ARRRRRRRRRRRRRRRRRRRRRRRR"<<exemplar_name<<endl<<endl;
            // abcGenerateAIGfromPLA(pa_path_str+exemplar_name+".train.pla",aigs_path+exemplar_name+"-complete.aig");
            cout << "Complete batch size:" << complete_batch_size <<endl;
            cout << "pla_filepath:" << pla_train_filepath << endl;

            // Setting up vectors
            vector<int> cols={250}; //sizes the CGP will be trained
            vector<int> batch_size_values={complete_batch_size}; // batch sizes to be trained, requirement: MULTIPLE OF 64
            // vector<int> batch_size_values={0.125*complete_batch_size,0.25*complete_batch_size, // batch sizes to be trained, requirement: MULTIPLE OF 64
            //                                 0.375*complete_batch_size,0.50*complete_batch_size,
            //                                 0.625*complete_batch_size,0.75*complete_batch_size,
            //                                 0.875*complete_batch_size
            //                                 }; 
            vector<int> change_each_values={num_generations}; // change_each values to be tested, to turn off use num_generations
            // vector<int> change_each_values={1000,2000,3000,4000,5000,10000,15000,20000,25000}; // change_each values to be tested, to turn off use num_generations
            vector<float> batch_change_percentage_values={1};

            // abcCallML(exemplar_name,pla_train_filepath+pla_valid_exten,aigs_path);
            // out_abc=abcReadData();
            // sbcci<<get<0>(out_abc)<<","<<get<1>(out_abc)<<","<<get<2>(out_abc)<<",";
            //middle loop, running multiple CGP sizes
            #if bootstrap_init == 0
            for(int d=0;d<cols.size();d++)
            {
                column_size=cols[d];
            #else
            {
                    init_type="Boots";
                    int d=0; //TODO FILE WILL BE READ AFTERWARDS
                    column_size=0;
            #endif
            #if improve_size == 0
                // loop to iterate over batch sizes
                for (int i = 0; i < batch_size_values.size(); i++)
                {
                    // loop to iterate over different change_each values
                    for (int j = 0; j < change_each_values.size(); j++)
                    {
                        // loop to iterate over batch_percentage
                        for (int k = 0; k < batch_change_percentage_values.size(); k++)
                        {       
                            batch_change_percentage = batch_change_percentage_values[k];
                            change_each = change_each_values[j];
                            pla_batch_size = batch_size_values[i];
            #else
            pla_batch_size=complete_batch_size;
            change_each = num_generations;
            {
                {
            #endif
                            cout<<"Starting colum size;"<<cols[d]<<", d:"<<d<<endl;

                            for(int seed_num=seed_init;seed_num<=seed_end;seed_num+=1)
                            {
                                
                                #if adaptative_batch == 1
                                init_batch_value = 64;
                                #else
                                init_batch_value = pla_batch_size;
                                #endif

                                finished_gen = num_generations;
                                #if random_seed == 0
                                    mt19937 mt(seed_num); //used to generate random numbers
                                    cout<<"Starting seed:"<<seed_num<<endl;
                                #else
                                    mt19937 mt(device());
                                    cout<<"Starting random seed, seed_num:"<<seed_num<<endl;
                                #endif
                                auto begin_solo = std::chrono::high_resolution_clock::now(); //start measuring time from here

                                my_CgpPopulation.clearCgp();  //Refresh CGP population and their AIG representation.
                                aig_popu_obj.clearAigPopu();  //Refresh CGP population and their AIG representation.
                                string learn_hyperparams=
                                        #if explicit_generations == 1
                                        ("--GEN"+to_string(num_generations)+
                                        #else 
                                        ("--MaxNoGain"+to_string(max_no_gain)+
                                        #endif
                                        "-S"+to_string(seed_num)+
                                        "-C"+to_string(column_size)+
                                        "-BS"+to_string(pla_batch_size)+
                                        "-CE"+to_string(change_each)+
                                        "-PER"+to_string(batch_change_percentage)+
                                        ".csv");
                                if(learning_file_name.find("--")==string::npos)
                                {
                                    learning_file_name.replace(learning_file_name.find_last_of("."),
                                        learning_file_name.size(),learn_hyperparams);
                                    cout<<learning_file_name<<endl;
                                }
                                else
                                {
                                    learning_file_name.replace(learning_file_name.find("--"),
                                        learning_file_name.size(),learn_hyperparams);
                                    cout<<learning_file_name<<endl;
                                }

                                cout<<"learning output file name:"<<learning_file_name<<endl;
                                learning_fitnes.close(); learning_fitnes.open(learning_file_name, ios::out);
                                if(learning_fitnes.is_open())
                                    cout<<"LEARNING FITNES OPEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
                                learning_fitnes<<"Generation,Best,mRate,FuncAnds,FuncPIs"<<endl;
                                cout<<"STARTING RANDOM INIT, d:"<<d<<endl;  //<<", change:"<<change<<endl;
                                whole_pla.clear();
                                cout<<"pla_train_filepath:"<<pla_train_filepath<<endl;
                                whole_pla.readPLA(pla_train_filepath,complete_batch_size);
                                // whole_pla.readPLA(pla_train_filepath,pla_batch_size);
                                whole_pla.setRandomBatch(mt, 1); //whole_pla.printBatch();
                                PIs=whole_pla.getPiSize(); //PI == Primary Input

                                //initializing internal data structure, for CGP and AIG populations
                                #if bootstrap_init == 1
                                    #if run_option == 2
                                        my_CgpPopulation.bootstrapInitialize(full_name,aigs_path,&whole_pla,&aig_popu_obj,mt);
                                    #else
                                        my_CgpPopulation.bootstrapInitialize(exemplar_name,aigs_path,&whole_pla,&aig_popu_obj,mt);
                                    #endif
                                #else
                                    my_CgpPopulation.createNodes(exemplar_name,num_individuals,PIs,column_size,line_size,mt); //generating a CGP population
                                    aig_popu_obj.CGPpopToAIGpop(&my_CgpPopulation,PIs,1,column_size*line_size,true); //turning the CGP population into an AIG population
                                #endif                        
                                cgp_size=aig_popu_obj.getAigFromPopulation(0)->getAnds()->size()-1;
                                cout<<"Virtual size:"<<cgp_size<<endl;
                                //checking how the randomly generated population goes with the trainig batch at hand
                                aig_popu_obj.evaluateScoresMyImplement(&whole_pla,0,mt); //"my implementation" uses this program's memory, you can use ABC's evaluation also

                                cout<<endl<<"Batch size from vec:"<<whole_pla.getBatch()->size()<<", batch size:"<<whole_pla.getBatchSize()<<endl;
                                cout<<"CGP SCORES "<<init_type<<","<<exemplar_name<<":";   aig_popu_obj.printScores();;
                                //            cout<<"GENERATED CGP:"; my_CgpPopulation.printGraph();
                                //            cout<<"GENERATED AIG:"; aig_popu_obj.getAigFromPopulation(0)->printCircuit();
                                mutation_chance=init_mut;
                                cout<<"Starting generations"<<endl;

                                #if DEBUG == 0
                                std::cout.setstate(std::ios_base::failbit);
                                #endif

                                #if explicit_generations == 1
                                //inner loop, the learning and the magic happens here!
                                for(int X=0;X<num_generations;X++)
                                {
                                #else
                                int X=0,previous_size=my_CgpPopulation.getSizes()[0],count_no_gain=0;
                                while(count_no_gain!=max_no_gain)
                                {
                                    X++;
                                #endif
                                    cout<<"Generation:"<<X<<",mutation chance:"<<(float)mutation_chance<<"%"<<endl;
                                    #if adaptative_batch == 1
                                    if ((X+1)%adaptative_change_interval == 0 && init_batch_value < pla_batch_size) {
                                        init_batch_value *= 2;
                                        // whole_pla.printBatch();
                                        whole_pla.setBatchSize(init_batch_value);
                                        whole_pla.setRandomBatch(mt, batch_change_percentage); //TODO test with 100% percentage
                                        cout<<"INIT BATCH VALUE"<<init_batch_value<<endl;
                                        // aig_popu_obj.evaluateScoresMyImplement(&whole_pla,1,mt); // verify if uses 1 or 0
                                        // whole_pla.printBatch();
                                    }
                                    #else
                                    whole_pla.setBatchSize(pla_batch_size);
                                    #endif
        //                            whole_pla.setRandomBatch(mt);
                                    //CGP object will hold the new father
                                    mutation_chance=my_CgpPopulation.generateOffspringOneToFive(&aig_popu_obj,mutation_chance,min_mutation,&whole_pla,change_each,mt, batch_change_percentage);
                                    aig_popu_obj.clearAigPopu();
                                    //generating offspring, their scores are calculated internally
                                    aig_popu_obj.CGPpopToAIGpop(&my_CgpPopulation,PIs,1,cgp_size,false);

                                    #if explicit_generations == 0
                                    if(previous_size-(my_CgpPopulation.getSizes()[0])==0)
                                        count_no_gain++;
                                    else
                                        count_no_gain=0;
                                    cout<<"count_no_gain:"<<count_no_gain<<", previous size:"<<previous_size<<", current:"<<my_CgpPopulation.getSizes()[0]<<endl;
                                    previous_size=(my_CgpPopulation.getSizes()[0]);
                                    #endif

                                    //writing and printing, tracking learning info from each generation
                                    partial_score=aig_popu_obj.getScores()->at(0);
                                    if(learning_fitnes.is_open())
                                        cout<<"learning_fitness is open!"<<endl;
                                    learning_fitnes<<X<<","<<aig_popu_obj.getScores()->at(0)<<","<<(float)mutation_chance/100<<","<<aig_popu_obj.getFuncSizes()->at(0)<<","<<aig_popu_obj.getFuncPis()->at(0)<<endl;
                                    cout<<"Best:"<<partial_score<<", #ANDs:"<<my_CgpPopulation.getSizes()[0]<<endl;
                                #if improve_size == 0                            
                                    if (partial_score >= 1) {
                                        valid_pla.clear(); valid_pla.readPLA(pla_path+exemplar_name+pla_valid_exten,0);
                                        int valid_batch_size_1 = valid_pla.getBatchSizeFile(pla_path+exemplar_name+pla_valid_exten);
                                        valid_pla.setBatchSize(valid_batch_size_1);
                                        aig_popu_obj.evaluateScoresMyImplement(&valid_pla,0,mt); 
                                        float valid_score_1=aig_popu_obj.getScores()->at(aig_popu_obj.getOrderedIndexes()->at(0));
                                        if (valid_score_1 >= 1) {
                                            finished_gen = X;
                                            break;
                                        } else {
                                            whole_pla.setRandomBatch(mt, batch_change_percentage);
                                        }
                                    }

                                #else
                                    if(my_CgpPopulation.getSizes()[0]<=10)
                                        {uniform_int_distribution<int>dist(2,20); sleep(dist(mt)); break;}
                                #endif
                                }
                                #if DEBUG == 0
                                        cout.clear();
                                #endif
                                cout<<"Finishing seed:"<<seed_num<<endl;
                                // // last generation evaluation, this is important to be done with VALIDATION data
                                valid_pla.clear(); valid_pla.readPLA(pla_path+exemplar_name+pla_valid_exten,0);
                                int valid_batch_size = valid_pla.getBatchSizeFile(pla_path+exemplar_name+pla_valid_exten);
                                valid_pla.setBatchSize(valid_batch_size);
                                aig_popu_obj.evaluateScoresMyImplement(&valid_pla,0,mt); 
                                float valid_score=aig_popu_obj.getScores()->at(0);
                                valid_pla.clear(); valid_pla.readPLA(pla_path+exemplar_name+pla_test_exten,0);
                                valid_batch_size = valid_pla.getBatchSizeFile(pla_path+exemplar_name+pla_test_exten);
                                valid_pla.setBatchSize(valid_batch_size);
                                aig_popu_obj.evaluateScoresMyImplement(&valid_pla,0,mt); 
                                float test_score=aig_popu_obj.getScores()->at(0);
                                float test_score2=aig_popu_obj.getAigFromPopulation(0)->getCurrentScore();
                                valid_pla.clear(); valid_pla.readPLA(pla_path+exemplar_name+pla_train_exten,0);
                                valid_batch_size = valid_pla.getBatchSizeFile(pla_path+exemplar_name+pla_train_exten);
                                valid_pla.setBatchSize(valid_batch_size);
                                aig_popu_obj.evaluateScoresMyImplement(&valid_pla,0,mt); 
                                partial_score=aig_popu_obj.getScores()->at(0);
                                cout<<"last score --> validation:"<<valid_score<<"test:"<<test_score<<endl;
                                string path=out_path;
                                string out_aig_name=exemplar_name
                                        +"acc"+to_string((int)(100000*test_score))
                                        +init_type
                                        +"size"+to_string(cgp_size)
                                        +"batch"+to_string(pla_batch_size)
                                        +"ce"+to_string(change_each)
                                        +"seed"+to_string(seed_num)
                                        +"can"+to_string(checkAllNodesAsPO)
                                        +"gens"+to_string(num_generations)
                                        +"bench"+to_string(new_benchmarks)
                                        +"mf"+to_string(XAIG)
                                        +"Origin.aig";
                                aig_popu_obj.getAigFromPopulation(0)->writeAIG(path,out_aig_name);
                                aig_popu_obj.getAigFromPopulation(0)->setDepthsInToOut();
                                // abcCallML(exemplar_name,pla_train_filepath+pla_valid_exten,aigs_path);
                                // out_abc=abcReadData(); 
                                // sbcci<<"original size,original level,original score,new size,new level,new score"<<endl;;
                                // sbcci<<get<1>(out_abc)<<","<<get<2>(out_abc)<<","<<get<3>(out_abc)<<",";;
                                //writing results
                                results.open("../../results.csv", ios::app);
                                results<<init_type<<","<<seed_num<<","<<cgp_size<<","<<change_each<<","<<exemplar_name<<","<<partial_score<<","<<valid_score<<","<<test_score<<","<<test_score2;//<<get<0>(scores_pair)<<","<<get<1>(scores_pair)<<","<<get<2>(scores_pair);//<<"("<<original_valid_2<<")";
                                results<<","<<(aig_popu_obj.getFuncSizes()->at(aig_popu_obj.getOrderedIndexes()->at(0)))<<","<<aig_popu_obj.getAigFromPopulation(0)->getDepth()<<",";
                                results<<aig_popu_obj.getAigFromPopulation(aig_popu_obj.getOrderedIndexes()->at(0))->getPIs()->size()-1<<",";
                                results<<(aig_popu_obj.getFuncPis()->at(aig_popu_obj.getOrderedIndexes()->at(0)))<<",";
                                auto finish_solo = std::chrono::high_resolution_clock::now();
                                elapsed = finish_solo-begin_solo;
                                results<<elapsed.count() << "," <<pla_batch_size;
                                #if run_option == 2
                                results<<","<<(full_name);
                                #endif
                                results<<",,"<< finished_gen<<","<<batch_change_percentage;
                                #if adaptative_batch == 1
                                results<<","<<adaptative_change_interval;
                                #else
                                results<<","<<0;
                                #endif
                                results<<endl;  results.close();
                                cout<<"Finishing colum size:"<<cols[d]<<", d:"<<d<<endl;
                            }

                            // scores_pair=aig_popu_obj.lastEvaluation(&whole_pla);
                            // cout<<"TRAIN SCORE:"<<get<0>(scores_pair)<<", VALID SCORE:"<<get<1>(scores_pair)<<", VALID_2 SCORE:"<<get<2>(scores_pair)<<endl;
                            // my_CgpPopulation.printGraph();
                            // aig_popu_obj.getAigFromPopulation(0)->printCircuit();
                            // aig_popu_obj.getAigFromPopulation(0)->writeAIG("./","output.aig"); //writing the generated AIG as a file
                        }
                    }
                }
            }
        }
#if INF_LOOP == 1
    }
#endif

    auto finish_all = std::chrono::high_resolution_clock::now();
    elapsed=finish_all-begin_all;
    results.open("../../results.csv", ios::app);
    results<<"Time to process everything:"<<elapsed.count()<<endl;
    results.close();
    learning_fitnes.close();
//    system("./createTogo.sh");
    return 0;
}
