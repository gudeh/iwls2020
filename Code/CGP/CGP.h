/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CGP.h
 * Author: user
 *
 * Created on April 3, 2020, 3:50 PM
 */

#ifndef CGP_H
#define CGP_H

#define write_times 0
//#define constant_seed 1

#include <vector>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <time.h>
#include <chrono>
#include <random>

#include "AIG.h"

using namespace std;


class AigPopulation;
class binaryPLA;

class CgpPopulation {
private:
//    vector<vector<NodeCgp>> the_CGPpopulation;
//    vector<NodeCgp> the_CGPpopulation;
    string name;
    vector<vector<short int> > the_CGPpopulation;
    unsigned int num_lines,num_columns,PIs,num_cgps;
    vector<float> all_scores_cgp;
    vector<int> ordered_indexes_cgp;
    vector<int> all_func_ands_cgp;
    vector<int> all_func_pis_cgp;
    int original_score_chosen,child_chosen;
//    float mean_p1_cgps;
//    vector<float> all_P1_cgps;
public:
    CgpPopulation();
    CgpPopulation(vector<vector<short int> > param, unsigned int PIs,unsigned int num_columns, unsigned int num_lines);
    CgpPopulation(const CgpPopulation& orig);
    virtual ~CgpPopulation();
    
    void createNodes(string name,unsigned int num_cgps_param, unsigned int PIs,unsigned int num_columns, unsigned int num_lines,mt19937& seed);
    void bootstrapInitialize(string exemplar_name,string path_to_aig,binaryPLA* PLA,AigPopulation* aig_pop,mt19937& mt);
    void generateFathersOldSelection(AigPopulation* my_pop,double number_childs,double mutation_chance,binaryPLA* my_pla,string PLA_file,int batch_threshold);
    //returns new mutation rate
    float generateOffspringOneToFive(AigPopulation* my_pop,double mutation_chance,double min_mut,binaryPLA* my_pla,int batch_threshold,mt19937& seed, float batch_change_percentage);
    void chooseChildren(AigPopulation* previous_population);

//    float getMeanP1Cgps();
//    void setMeanP1Cgps(float mean_p1_cgps);

    void setVector(vector<vector<short int> > param);
    void setScores(vector<float>* param);
    void setOrderedIndexes(vector<int>* param);

    //retrieves the ith CGP individual
    vector<short int>* getVector(int ith_individual);
    vector<float> getScores();
    vector<int> getOrderedIndexes();
    vector<int> getSizes();
    vector<int> getFuncPis();
    int getNumLines();
    int getNumCols();
    int getPopulationSize();
    string getName();
    
    void writeCgps();
    void clearCgp();
    void printGraph();
    

};
#endif /* CGP_H */

