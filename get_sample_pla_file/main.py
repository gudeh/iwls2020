import pandas as pd

if __name__ == '__main__':
    # Skip first lines and the last for extracting only the useful data for samples
    df = pd.read_csv('ex41.train.pla', skiprows=4, skipfooter=1, engine='python')
    df = pd.DataFrame.drop_duplicates(df)

    for x in range(10, 110, 10):
        output = df.sample(frac=x/100)
        filepath = 'samples/{}_percent_sample.train.pla'.format(x)

        # Add header
        with open(filepath, 'w') as file:
            file.write('.i 10\n.o 1\n.p {}\n.type fr\n'.format(len(output)))
        # Add data from samples
        output.to_csv(filepath, index=False, header=None, mode='a')
        # Add footer
        with open(filepath, 'a') as file:
            file.write('.e\n')
